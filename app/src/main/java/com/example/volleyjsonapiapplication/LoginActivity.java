package com.example.volleyjsonapiapplication;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.volleyjsonapiapplication.AESUtils;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.Call;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class LoginActivity extends AppCompatActivity {

    private static final String KEY_STATUS = "status";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_USERNAME = "167328";
    private static final String KEY_PASSWORD = "lorem@2016";

    private String username;
    private String password;

    String baseUrl = "https://jsonplaceholder.typicode.com/posts";
    String url;  // This will hold the full URL which will include the username entered in the etGitHubUser.

    RequestQueue requestQueue;

    EditText edtUsername;
    EditText edtPassword;
    Button btnLogin;
    TextView cryptId;
    TextView tvRepoList;  // This will reference our repo list text box.
//    UserService userService;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        edtUsername = (EditText) findViewById(R.id.edtUsername);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        this.tvRepoList = (TextView) findViewById(R.id.tv_repo_list);  // Link our repository list text output box.


        cryptId = (TextView) findViewById(R.id.cryptUserid);

        requestQueue = Volley.newRequestQueue(this);  // This setups up a new request queue which we will need to make HTTP requests.

//        userService = ApiUtils.getUserService();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String username = edtUsername.getText().toString();
//                String password = edtPassword.getText().toString();

                //validate form
//                if(validateLogin(username, password)){
//                    //do login
////                    doLogin(username, password);
//                    login();
//                }
                login();
            }
        });

    }


    public void addToDoItem(View view) {
        EditText editText = findViewById(R.id.editText);
        final String message = editText.getText().toString();

        try {
            final Context context = this;
            JSONObject newPost = new JSONObject();
            newPost.put("message", message);

//            String url = "https://jsonplaceholder.typicode.com/posts";
//            String url = "http://www.mocky.io/v2/597c41390f0000d002f4dbd1";
            String url = "http://124.30.44.228/ETMSMOBILE.Api/api/login";
//            JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, url, newPost,
//                    new Response.Listener<JSONObject>() {
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                        @Override
//                        public void onResponse(JSONObject response) {
                        public void onResponse(String response) {
//                            startActivity(new Intent(context, MainActivity.class));

                            try {
//                                JSONObject userJson = response.optJSONObject("name");
                                new AlertDialog.Builder(context)
                                        .setTitle("onResponse if - length : ")
                                        .setMessage(response)
                                        .show();
                            } catch (Exception e) {
                                    e.printStackTrace();
                                }

//                            tvRepoList.setText(response.optString("users"));


//                            if (response.length() > 0) {
//                                JSONArray jsonarray;
//                                try {
//                                    jsonarray = new JSONArray(response);
//                                    for (int i = 0; i < jsonarray.length(); i++) {
//                                        JSONObject mJsonObject = jsonarray.optJSONObject(i);
//
//                                        if(mJsonObject.getString("name").equals(message))
//                                        {
//                                            new AlertDialog.Builder(context)
//                                                    .setTitle("onResponse if - length : " + response.length())
//                                                    .setMessage(mJsonObject.get("name").toString() + "\n" + mJsonObject.get("email").toString())
//                                                    .show();
//                                        }
//                                        else
//                                        {
//                                            new AlertDialog.Builder(context)
//                                                    .setTitle("onResponse else - length : " + response.length())
//                                                    .setMessage(mJsonObject.get("name").toString())
//                                                    .show();
//                                        }
//
//                                    }
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            new AlertDialog.Builder(context)
                                    .setTitle("Error")
                                    .setMessage(error.getMessage())
                                    .show();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    return headers;

                }

                            @Override
                            protected Map<String, String> getParams() {

                                // Creating Map String Params.
                                Map<String, String> params = new HashMap<String, String>();

                                // Adding All values to Params.
                                // The firs argument should be same sa your MySQL database table columns.
                                params.put("EmployeeId", "167328");
                                params.put("Password", "Lorem@2012");

                                return params;
                            }
            };

            // Add the request to the RequestQueue.
            RequestQueue queue = Volley.newRequestQueue(context);
            queue.add(stringRequest);
        } catch (JSONException e) {
            System.out.println(e.getMessage());
        }
    }





    private boolean validateLogin(String username, String password){
        if(username == null || username.trim().length() == 0){
            Toast.makeText(this, "Username is required", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(password == null || password.trim().length() == 0){
            Toast.makeText(this, "Password is required", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

//    private void doLogin(final String username,final String password){
//        Call call = userService.login(username,password);
//        call.enqueue(new Callback() {
//            @Override
//            public void onResponse(Call call, Response response) {
//                if(response.isSuccessful()){
//                    ResObj resObj = response.body();
//                    if(resObj.getMessage().equals("true")){
//                        //login start main activity
//                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//                        intent.putExtra("username", username);
//                        startActivity(intent);
//
//                    } else {
//                        Toast.makeText(LoginActivity.this, "The username or password is incorrect", Toast.LENGTH_SHORT).show();
//                    }
//                } else {
//                    Toast.makeText(LoginActivity.this, "Error! Please try again!", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call call, Throwable t) {
//                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

    private void doLogin(final String username,final String password){

        String encrypted = "";
        String sourceStr = username;
        try {
            encrypted = AESUtils.encrypt(sourceStr);
            Toast.makeText(this, "encrypted is " + encrypted, Toast.LENGTH_SHORT).show();
            cryptId.setText(encrypted);
            Log.d("TEST", "encrypted:" + encrypted);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void login() {
//        displayLoader();
        JSONObject request = new JSONObject();
        try {
            //Populate the request parameters
            request.put(KEY_USERNAME, username);
            request.put(KEY_PASSWORD, password);

        } catch (JSONException e) {
            e.printStackTrace();
        }
//        JsonObjectRequest jsArrayRequest = new JsonObjectRequest
//                (Request.Method.POST, baseUrl, request, new Response.Listener<JSONObject>() {
                    JsonArrayRequest arrReq = new JsonArrayRequest(Request.Method.POST, baseUrl, null,
                            new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        if (response.length() > 0) {
                            // The user does have repos, so let's loop through them all.
                            for (int i = 0; i < response.length(); i++) {
                                try {
                                    // For each repo, add a new line to our repo list.
                                    JSONObject jsonObj = response.getJSONObject(i);
                                    String repoName = jsonObj.get("userId").toString();
                                    String lastUpdated = jsonObj.get("title").toString();
                                    addToRepoList(repoName, lastUpdated);
                                } catch (JSONException e) {
                                    // If there is an error then output this to the logs.
                                    Log.e("Volley", "Invalid JSON Object.");
                                }

                            }
                        } else {
                            // The user didn't have any repos.
                            setRepoListText("No repos found.");
                        }

//                        pDialog.dismiss();
//                        try {
//                            //Check if user got logged in successfully
//
//                            cryptId.setText(response.toString());
//                            Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_SHORT).show();
//
//                            if (response.getInt(KEY_STATUS) == 0) {
////                                session.loginUser(username,response.getString(KEY_FULL_NAME));
////                                loadDashboard();
//                                cryptId.setText("Check if user got logged in successfully");
//
//                            }else{
//                                Toast.makeText(getApplicationContext(), response.getString(KEY_STATUS), Toast.LENGTH_SHORT).show();
//                                cryptId.setText(response.getString(KEY_STATUS));
//
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
                    }
                },

                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // If there a HTTP error then add a note to our repo list.
                                    setRepoListText("Error while calling REST API");
                                    Log.e("Volley", error.toString());
                                }
                            });
//                            new Response.ErrorListener() {
//
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
////                        pDialog.dismiss();
//
//                        //Display error message whenever an error occurs
////                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getApplicationContext(), "error.getMessage", Toast.LENGTH_SHORT).show();
//                        cryptId.setText(error.toString());
//                    }
//                });

        // Access the RequestQueue through your singleton class.
//        MySingleton.getInstance(this).addToRequestQueue(jsArrayRequest);
        requestQueue.add(arrReq);
    }

    private void addToRepoList(String repoName, String lastUpdated) {
        // This will add a new repo to our list.
        // It combines the repoName and lastUpdated strings together.
        // And then adds them followed by a new line (\n\n make two new lines).
        String strRow = repoName + " / " + lastUpdated;
        String currentText = tvRepoList.getText().toString();
        this.tvRepoList.setText(strRow);
    }


    private void setRepoListText(String str) {
        // This is used for setting the text of our repo list box to a specific string.
        // We will use this to write a "No repos found" message if the user doens't have any.
        this.tvRepoList.setText(str);
    }

}
